from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList
from todos.forms import TodoForm

# Create your views here.
def delete_list(request, id):
    to_do_list = TodoList.objects.get(id=id)
    if request.method == "POST":
        to_do_list.delete()
        return redirect("todos_list")
    return render(request, "todos/delete.html")


def edit_list(request, id):
    to_do_list = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=to_do_list)
        if form.is_valid():
            form.save()
            return redirect("todos_edit", id=id)
    else:
        form = TodoForm(instance=to_do_list)
    context = {
        "to_do_list": to_do_list,
        "form": form,
    }
    return render(request, "todos/edit.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            TodoList = form.save()
            return redirect("todos_list_detail", id=TodoList.id)
    else:
        form = TodoForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_list(request):
    to_do_list = TodoList.objects.all()
    context = {"to_do_list": to_do_list}
    return render(request, "todos/list.html", context)


def todos_list_detail(request, id):
    to_do_list = get_object_or_404(TodoList, id=id)
    context = {"to_do_list": to_do_list}
    return render(request, "todos/detail.html", context)
