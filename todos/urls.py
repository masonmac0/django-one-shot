from django.urls import path
from todos.views import (
    todo_list_list,
    todos_list_detail,
    create_list,
    edit_list,
    delete_list,
)

urlpatterns = [
    path("", todo_list_list, name="todos_list"),
    path("<int:id>/", todos_list_detail, name="todos_list_detail"),
    path("create/", create_list, name="todos_create"),
    path("<int:id>/edit/", edit_list, name="todos_edit"),
    path("<int:id>/delete/", delete_list, name="todos_delete"),
]
